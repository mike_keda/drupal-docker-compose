# Drupal Kit

## Quick start

First, you need to have a [Docker and Docker Compose](https://www.docker.com/) installed.

For OS X users I would also recommend to have a [docker-machine-nfs](https://github.com/adlogix/docker-machine-nfs/).

```bash
docker-compose up
```
